# Serve SPA

This Repo contains the source for the docker image https://hub.docker.com/r/zottelchin/serve-spa.

It creates a Docker-Container for Single Page Applications. It's based on the Caddy webserver and is configured to:
- proxy everything to `/api/*` to a other websever set by env `PROXY_SERVER`
- serve all files mounted to `/srv`
- serve the `ìndex.html` file, when there is no file matching the path